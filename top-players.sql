select
    t.TeamName as team_name,
    p.FirstName || ' ' || p.LastName as player,
    max(p.NumGoals) as score
from
    Teams t
    left outer join Players p on (t.TeamID = p.TeamID)
group by
    t.TeamId
order by
     score asc
;
