select
    t.TeamName as team_name,
    sum(p.NumGoals) as score
from
    Teams t
    left outer join Players p on (t.TeamID = p.TeamID)
group by
    t.TeamId
having
    (score >= 30)
order by
     score desc
;
